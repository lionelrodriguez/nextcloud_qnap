<?php
$CONFIG = array (
  'htaccess.RewriteBase' => '/',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/var/www/html/apps',
      'url' => '/apps',
      'writable' => false,
    ),
    1 => 
    array (
      'path' => '/var/www/html/custom_apps',
      'url' => '/custom_apps',
      'writable' => true,
    ),
  ),
  'instanceid' => 'oclwtg9gmxaf',
  'passwordsalt' => 'scozt6/dMEUP7V3G902RBr2pNG1NSj',
  'secret' => 'tOJZYbD2KMIldaozIDmOq31xsmDV8Xm7stkFs3jRA7D9XQSI',
  'trusted_domains' => 
  array (
    0 => '192.168.1.250',
    1 => '172.30.0.1',
  ),
  'datadirectory' => '/var/www/html/data',
  'dbtype' => 'mysql',
  'version' => '17.0.0.9',
  'overwrite.cli.url' => 'http://192.168.1.250:12520',
  'dbname' => 'nextcloud',
  'dbhost' => '172.30.1.2:3306',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'mysql.utf8mb4' => true,
  'dbuser' => 'nextcloud',
  'dbpassword' => 'nextcloud',
  'installed' => true,
  'trusted_proxies' => 
  array (
    0 => '192.168.1.250',
    1 => 'localhost',
  ),
  'overwritewebroot' => '/',
  'overwritehost' => '192.168.1.250:448',
  'overwriteprotocol' => 'https',
  'maintenance' => false,
  'theme' => '',
  'loglevel' => 2,
);
