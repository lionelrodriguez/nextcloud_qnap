# Nextcloud_QNAP
Instalacion Nextcloud solo para una red interna local y llenarlo de basura.

Reemplazar la ip por la actual en el archivo nextcloud_qnap/proxy/conf.d/ngnix.conf

Cambiar los permisos de los directorios y del usuario wwww-data

docker-compose exec app chown www-data.root /var/www/html
docker-compose exec app chown www-data.root * /var/www/html/config
docker-compose exec app chown www-data.root * /var/www/html/data
docker-compose exec app chmod 770 /var/www/html/data
docker-compose exec app ls -l /var/www/html

Renombrar el archivo nextcloud_qnap/app/config.php para que no lo tome y crear un archivo vacío llamado CAN_INSTALL

mv -v ./nextcloud_qnap/app/config/config.php ./nextcloud_qnap/app/config/config.old
touch CAN_INSTALL

Apuntar a https://<IP>:448/nextcloud y completar la instalacion en base a valores simlares del archvo ./nextcloud_qnap/app/config/config.old

Antes de ejecutar la instalacion, agregar lo siguiente al final de la declaracion final del archivo nextcloud_qnap/app/config/config.php que se generó con la instalación para que al final de la instalacion no tire un error de proxeado:

  'overwrite.cli.url' => 'http://192.168.1.250:12520',

); <-- Tiene que ir antes de esto!

El parámetro, Databse Host tiene que ser: 172.30.1.2:3306 para la instalación)

Mantenimiento:

Borrar la base de datos:

docker-compose exec --user root db /usr/bin/mysql -unextcloud -pnextcloud -e 'DROP DATABASE nextcloud;'

docker exec --user root -t CONTAINER_ID_HERE /opt/bitnami/mariadb/bin/mysql -uroot -pPASSWORD_GOES_HERE -e 'DROP DATABASE bitnami_ghost;'
